"""Helper functions for hound"""
import re
import os
import subprocess
import requests
import unidiff
import yaml


class Helper(object):
    """
    Helper Functions:
    comment, get_diff, check_style
    """
    def __init__(self, data):
        self.data = data
        self.headers = {'PRIVATE-TOKEN': data['GITLAB_PRIVATE_TOKEN']}
        self.find_errors()

    def comment_on_mr(self):
        """Comment on Mr with errors
        """
        mr_notes_url = "{}/api/v4/projects/{}/merge_requests/{}/notes".format(self.data['GITLAB_URL'], self.data['project_id'], self.data['mr_id'])
        payload = {'body': self.data['comment_message']}
        resp = requests.post(mr_notes_url, headers=self.headers, data=payload)
        return resp.status_code

    def get_mr_diff(self):
        """get all python and javascript diff files with line numbers
        """
        mr_url = "{}/api/v4/projects/{}/merge_requests/{}/changes".format(self.data['GITLAB_URL'], self.data['project_id'], self.data['mr_id'])
        resp = requests.get(mr_url, headers=self.headers)
        mr_files = {}
        for change in resp.json().get('changes', []):
            patch = unidiff.PatchSet(change.get('diff').splitlines(), encoding=resp.encoding)
            for patchset in patch:
                if patchset.target_file[-3:] == '.py':
                    file_name = patchset.target_file[1:]
                    mr_files[file_name] = []
                    for hunk in patchset:
                        for line in hunk.target_lines():
                            if line.is_added:
                                mr_files[file_name].append(line.target_line_no)
        self.data['mr_files'] = mr_files
        return mr_files

    def get_hound_config(self):
        """reading .hound.yml file
        """
        hound_config = {
            'ignore_files': [],
            'no_errors_comment': True,
            'python': {
                'use': {
                    'pycodestyle': {
                        'options': "--max-line-length=160"
                    },
                    'prospector': {
                        'profile_path': ".prospector.yml"
                    },
                },
                'run_prospector': False
            }
        }
        hound_file = "{}/projects/{}/repository/files/{}/raw?ref={}".format(self.data['GITLAB_URL'], self.data['project_id'], '%2Ehound%2Eyml', self.data['source_branch'])
        cmd = "curl -k --header 'PRIVATE-TOKEN: {}' '{}'".format(self.data['GITLAB_PRIVATE_TOKEN'], hound_file)
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        stdout, _ = proc.communicate()
        stdout = stdout.decode("utf-8")
        if '404' not in stdout:
            try:
                hound_config = yaml.load(stdout)
            except yaml.YAMLError:
                pass
        return hound_config

    def run_style_check_py(self):
        """Run style check on each file...
        """
        config = self.get_hound_config()
        mr_files = self.get_mr_diff()
        self.data['all_results'] = {}
        self.data["results"] = {}
        if config.get('python').get('run_prospector') is True:
            pass
        else:
            check_cmd = "pycodestyle {} raw_file.py".format(config.get('python', {}).get('pycodestyle', {}).get('options', ''))

        for mr_file in mr_files:
            if mr_file[0] == '/':
                mr_file_name = mr_file[1:]
            enc_file_name = mr_file_name.replace('.', '%2E').replace('/', '%2F').replace('-', '%2D').replace('_', '%5F')
            raw_file_url = "{}/projects/{}/repository/files/{}/raw?ref={}".format(self.data['GITLAB_URL'], self.data['project_id'], enc_file_name, self.data['source_branch'])
            cmd = "curl -k --header 'PRIVATE-TOKEN: {}' '{}'".format(self.data['GITLAB_PRIVATE_TOKEN'], raw_file_url)
            proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            stdout, _ = proc.communicate()
            with open("raw_file.py", 'wb') as raw_file:
                raw_file.write(stdout)
            if config.get('python').get('run_prospector') is True:
                # Not implimented
                pass
            else:
                proc = subprocess.Popen(check_cmd, shell=True, stdout=subprocess.PIPE)
                stdout, _ = proc.communicate()
                self.data["all_results"][mr_file] = stdout.decode('utf-8').splitlines()
                self.data["results"][mr_file] = []
                for error in list(self.data["all_results"][mr_file]):
                    if re.search("^raw_file.py:\d+:\d+:\s[WE]\d+\s.*", error):
                        self.data["results"][mr_file].append(error.replace("raw_file.py", mr_file))
                ## Remove errors which are not in mr
                for error in list(self.data["results"][mr_file]):
                    if not int(error.split(":")[1]) in mr_files[mr_file]:
                        self.data["results"][mr_file].remove(error)
                os.remove("raw_file.py")

    def find_errors(self):
        self.run_style_check_py()
        self.prepare_comment_message()
        self.comment_on_mr()

    def prepare_comment_message(self):
        errors = False
        comment_message = []
        for mr_file, issues in self.data["results"].items():
            if not len(issues) == 0:
                errors = True
                comment_message.append(
                    "In **{}** file. I found below errors:".format(mr_file)
                )
                for issue in issues:
                    error_string = issue.replace(mr_file + ":", "Line ")
                    comment_message.append("\n\n     {0}".format(error_string))
                comment_message.append("\n\n")
        if not errors:
            comment_message = ["No errors. Everything looks good to me."]
        comment_message = ''.join(comment_message)
        comment_message = "Hi @{} \n\n".format(self.data['author']) + comment_message + "\n\n -- Hound"
        self.data['comment_message'] = comment_message

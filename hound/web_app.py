import os
import json
from flask import Flask, request
from helpers import Helper

app = Flask(__name__)

@app.route('/')
def about_me():
    """Index page of hound
    """
    return 'Hi, I am Hound. Visit https://gitlab.com/micropyramid/hound to Know More about me.'


@app.route('/mr-webhook/', methods=['POST', 'GET'])
def mr_event_catch():
    """hit this url when merge request events occured
    """
    if request.method == "POST":
        data = json.loads(request.data.decode('utf-8'))
        mr_data = {
            'project_id': data.get('object_attributes').get('target_project_id'),
            'mr_id': data.get('object_attributes').get('iid'),
            'target_branch': data.get('object_attributes').get('target_branch'),
            'source_branch': data.get('object_attributes').get('source_branch'),
            'state': data.get('object_attributes').get('state'),
            'action': data.get('object_attributes').get('action'),
            'author': data.get('user').get('username'),
            'oldrev': data.get('object_attributes').get('oldrev'),
            'work_in_progress': data.get('object_attributes').get('work_in_progress')
        }
        if os.environ.get('GITLAB_PRIVATE_TOKEN'):
            mr_data['GITLAB_PRIVATE_TOKEN'] = os.environ.get('GITLAB_PRIVATE_TOKEN')
        else:
            return "set GITLAB_PRIVATE_TOKEN in environment"
        if os.environ.get('GITLAB_URL'):
            mr_data['GITLAB_URL'] = os.environ.get('GITLAB_URL')
        else:
            return "set GITLAB_URL in environ"
        if mr_data['action'] == "open":
            Helper(mr_data)
            return "Job executed successfully"
        elif mr_data['action'] == "update" and mr_data['oldrev']:
            Helper(mr_data)
            return "Job executed successfully"
    return "Hound failed to find errors"


if __name__ == '__main__':
    app.run(port=8000)